%{
int count=1;
%}
%START COMMENT CHAR
ID [a-zA-Z_][a-zA-Z0-9]*
TYPE "int"|"double"|"char"
SYMBOL "("|")"|"{"|"}"|";"|","
OPERATOR "+"|"-"|"*"|"/"|"="|"++"|"--"|"<"|">"|"=="|"<="|">="|"!="
NUMBER [0-9]*

%%

<INITIAL>"/*"           BEGIN COMMENT;
<COMMENT>"\n"			count++;
\n						count++; //Record lines here
{TYPE}					printf("#%-2d data type  : %s\n\n",count,yytext);
<COMMENT>"*/"			BEGIN INITIAL;
<COMMENT>[^"\n"+^*/]*	printf("#%-2d comment\n\n",count);//block
"//".*					printf("#%-2d comment\n\n",count);//one line
"return"				printf("#%-2d %s\n\n",count,yytext);
<CHAR>\'                {
                            printf("#%-2d %s\n\n",count,yytext);
                            BEGIN INITIAL;
                        }

<CHAR>.                 printf("#%-2d character  : %s\n\n",count,yytext);
"for("|"while("|"if("	{
							unput(yytext[yyleng-1]);
							yytext[yyleng-1] = '\0';
							printf("#%-2d %s\n\n",count,yytext);
						}
						
{ID}"("					{
							unput(yytext[yyleng-1]);
							yytext[yyleng-1] = '\0';
							printf("#%-2d function ID: %s\n\n",count,yytext);
						}
"else"					printf("#%-2d %s\n\n",count,yytext);
{ID}					printf("#%-2d variable ID: %s\n\n",count,yytext);
{SYMBOL}				printf("#%-2d %s\n\n",count,yytext);
{OPERATOR}				printf("#%-2d operator   : %s\n\n",count,yytext);
{NUMBER}				printf("#%-2d number     : %s\n\n",count,yytext);
\'.\'					{	//declare a character
							unput(yytext[2]);
							unput(yytext[1]);
							yytext[1] = '\0';
							printf("#%-2d %s\n\n",count,yytext);
							BEGIN CHAR;
						}
.						;

%%						

int main(){
	yylex();
	return 0;
}
